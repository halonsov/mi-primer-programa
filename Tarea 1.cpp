#include <stdio.h>
#include <stdlib.h>

//Una gran disculpa por mi exelente ortografia y el hecho que debido a ciertos problemas ya reueltos no me ha sido posible asistir a clases , lo cual no implica el abandono de la materia
int main(){
           char seleccion_general;
 //en toooooda la tarea tuve el problema de no saber como evitar que el programa continue cuando se introducen caracteres no enteros
 //analogamente en toda la tarea , sin la precentacion  y el ejercicio 5 resiben enteros y regresan enteros s
 //en la precentacion resibe caracteres y solo sirve de transportador 
 //en las matrices se definen areglos de enteros de 5x5
 //tambien he de aclarar que en ciertas funciones triviales como los ciclos for para imprimir en pantalla , las funciones ya explicadas anteriormente y los goto (ve a) se omite la explicacion   
precentacion:
             puts("Bienvenido a la tarea 1\nEn esta calcularemos:\n1.-Las combinaciones de n elementos tomadas de en m en m\n2.-Los primeros n numeros de la serie de Fibonacci\n3.-El MCM y MCD de dos numeros dados\n4.-La descompocicion en factores primos de un numero dado\n5.-La traza(tr(A)), transpuesta y el cuadrado de una matriz dada A de 5x5");
             printf("Ingrese que quiere hacer 1 para el ejercicio 1, 2 para el ejercicio 2 ,analogamente para los ejercicios 3,4,5: ");
             scanf("%i", &seleccion_general);
             getchar();
             system("cls");
             if(seleccion_general==1) goto ejercicio_1;
             if(seleccion_general==2) goto ejercicio_2;
             if(seleccion_general==3) goto ejercicio_3;
             if(seleccion_general==4) goto ejercicio_4;
             if(seleccion_general==5) goto ejercicio_5;
             else {
                   puts("Veo que no sabe leer\nPorfavor ingrese un valor correcto");
                   goto precentacion;
                   }                         

                   
ejercicio_1:
            int n, m, factorial_n, j, factorial_m, k, factorial_ab_a, ab_a, arriba, seleccion_1, abajo, combinacion, i;//pare evitar confuciones se declaran las variables en cada ejercicio
            puts("Calcularemos las combinaciones de n elementos tomados de m en m \nDonde 0 <= m <= n");
            puts("Precione enter continuar");
            getchar();
            system("cls");
            printf("Introdusca el valor de n: ");
            scanf("%i", &n);
            getchar();
            system("cls");
            printf("Introdusca el valor de m: ");
            scanf("%i", &m);
            getchar();
            system("cls");
            goto verificar_nm;
//este ejercicio esta vasado en la formula de las combinaciones que ocupa factorial
verificar_nm:
             if(m<0){//*
                     puts("Veo que ha seleccionado al menos un valor negativo\nIngrese un valor correcto");
                     goto ejercicio_1;
                     }
             else if(m<n) goto inicio_factorial_n; 
             else if(m==n){//esta parte esta apartada del calculo de factoriales por si llegase a darce el caso en el que intrduscan los valores de n =m pero mayores que 13 
                          printf("m=%i es igual que n=%i\nPor lo cual las combinaciones de\n%i elementos tomados de %d en %d es 1\n", m, m, m, m, m);
                          goto fin_combinaciones;
                          }
             else {
                  puts("Se ha seleccionado una opcion incorrecta");//* y esta parte me eseguran que no llegue a la parte de calcular factiriaes a menos que m<n y positivos ambos , por el hecho que m a lo mas toma el valor de n, si m es mayor o igual que cero n tambien lo es
                  goto ejercicio_1;
                  }
                                   
inicio_factorial_n:  // esta limitante esta impuesta por que al elejir valores mayores que 13 el factorial sale un valor negativo supongo que es debido que los bits son finitos jejejeje                                   
                   if(n>12){
                            puts("el numero es muy grande para este programa porfavor elija un numero menor a 13"); 
                            goto ejercicio_1;
                            }
                   i=n;
                   factorial_n=1;
                   while(0<i) factorial_n*=i--;//en esta parte es claro que va multiplicando de mayor a menor hasta llegar al 1
                   goto inicio_factorial_m;    
                                                             
inicio_factorial_m:                                                    
                   j=m;
                   factorial_m=1;
                   while(0<j) factorial_m*=j--;
                   goto calcular_combinaciones;  
                   
calcular_combinaciones:
                       arriba=factorial_n;
                       k=n-m;
                       factorial_ab_a=1;
                       while(k>0) factorial_ab_a*=k--;// este es el factorial de n-m
                       abajo=factorial_m*factorial_ab_a;
                       combinacion=arriba/abajo;
                       goto imprimir;
                      
imprimir:         
         printf("Las combinaciones de\n%i elementos tomadas de %i en %i es %i\n", n, m, m, combinacion);
         goto fin_combinaciones;
                                                                                  
fin_combinaciones:
                  puts("Si desea continuar calculando combinaciones precione 0\nSi deseair al menu principal precione 1\nSi desea salir precione cualquier otro numero");
                  scanf("%i", &seleccion_1);
                  if (seleccion_1==0){
                                      system("cls");
                                      goto ejercicio_1;                            
                                      } 
                  if (seleccion_1==1){
                                      system("cls");
                                      goto precentacion;
                                      }                                       
                  else {
                        system("cls");
                        goto fin_general;
                        }               
//el mayor problema que tuve en este ejercicio (que por cierto aun sigo sin explicar me como fue que con un simple reordenamiento del codigo funciono el programa) fue el hecho que cuando el usuario queria volver a correl el programa los valores de m y n no se borraban por asi dcirlo y no siempre calculaba el factorial por el limitante de ser menor a 13 m y n            
//a si que para los ejercicios siguientes trate de seguir el orden del ejercicio 1                                         


ejercicio_4:
            int Q, H, F, seleccion_4, temp;
            puts("Se va a calcular la descompocicion en factores primos de n; con n no cero");
            printf("Ingrese el valor de n:  ");
            scanf("%d", &Q);
            system("cls");
            if (Q<0){
                      printf("%d=", Q);
                      Q=-Q;//esto es para aplicar el mismo metodo para el caso positivo
                      F=Q;
                      H=2;
                      while (H<=F){//comienza en 2                                 
                                  if (Q%H==0){//esto dice si el residuo de el cocienteQ/H es cero
/*
basicamente lo que hace este if es:
Si el residuo de el cociente Q/H es cero dividimos Q/H , hacemos Q=(Q/H) e imprimimos H*
*/                                               
                                              Q=(Q/H);                                              
                                              printf("%d * ", H);
                                              temp=temp+1;//este de aqui es usado para ver cuantas veces se dividio Q , si solo se divide una vez implica que el numero es primo , pues solo pudo ser dividido entre el mismo
                                              }           
                                  else H=H+1;      
/*este else es muy impotante ya que  al salir del if comienza de nuevo el ciclo para el mismo valor de H se verifica si el nuevo Q es otra vez divisible entre H si ya no lo es hacemos H=H+1
como el ciclo while lo comenzamos en 2 buscara si el numero es divisible entre 2 e impromira 2* tantas veces como sea divisible Q entre 2
hace H=H+1 ahora H toma el valor de 3 hace lo mismo
hece H=h+1 , ahora H toma el valor de 4 pero como nuestro nuevo Q ya no es divisible entre 2 tampoco lo es entre 4 
y asi analogamente hasta llegar a H=F alli termina el ciclo
es un proseso similar al empleado en la criba de eratostenes
*/
                                  }                                  
                      printf("-1\n");//y aqui se multiplica por -1 por el hecho de que Q original es negativo
                      goto fin_primo;
                     }
            if (Q>1){
                     printf("%d=", Q);
                     F=Q;
                     H=2; 
                     temp=0;
                     while (H<=F){
                                 if (Q%H==0){
                                             Q=(Q/H);
                                             printf("%d * ", H);
                                             temp=temp+1;
                                             }
                                 else H=H+1;
                                 
                                 }               
                     printf("1\n");
                     goto fin_primo;
                     }
            else {
                 puts("Ha seleccionado un valor incorrecto");//por cualquier otra cosa
                 goto ejercicio_4;
                 }

fin_primo:
          if (temp==1) puts("El numero es primo");
          puts("Si desea continuar calculando factores primos precione 0\nSi deseair al menu principal precione 1\nSi desea salir precione cualquier otro numero");
          scanf("%i", &seleccion_4);
          if (seleccion_4==0){
                              system("cls");
                              goto ejercicio_4;                            
                              } 
          if (seleccion_4==1){
                              system("cls");
                              goto precentacion;                                                           
                              }                                       
          else {
               goto fin_general;
               system("cls");               
               }         
            
//como se pueden dar cuenta el ejercicio 3 esta completamente vasado en el ejercicio 4 , ya que el MCD es igual a la multiplicacion de los primos comunes cada primo elevado al minimo exponente y el MCM es igual a la multiplicacion de los don numeros dividido entre el MCD
//asi que por el mismo motivo no habra mucha explicacion ya que ya fue explicado en el ejercicio 4                        
ejercicio_3:         
            int C, B, L, G, R, T, MCD, MCM, seleccion_3, tempSC, tempMC, tempSB, tempMB, tempC, tempB, temp_MCD, P, O, X, Y, tempMCD, V;            
            puts("Calcularemos el MCM y MCD de a y b positivos");
            printf("Ingrese el valor de a: ");
            scanf("%d", &C);
            getchar();
            system("cls");
            
ingresar_B:     
            printf("Ingrese el valor de b: ");
            scanf("%d", &B);
            getchar();
            system("cls");

verificar_p_A:
              if(0<C) goto verificar_p_B;
              else {
                    puts("Ingrese un valor positivo");
                    goto ejercicio_3;
                    }

verificar_p_B:
              if(0<C) goto calcular_p_A_B;
              else {
                    puts("Ingrese un valor positivo");
                    goto ingresar_B;
                    }
                    
calcular_p_A_B:
             L=C;
             G=2;
             R=B;
             T=2;
             tempC=0;
             tempB=0;
//obtenemos los factores primos de los numeros A y B que se llaman C y B respectivamente         
             while (G<=L){
                          if (C%G==0){
                                      C=(C/G);
                                      tempC=tempC+1;                                      
                                      }
                          else G=G+1;                                                          
                          }             
             while (T<=R){
                          if (B%T==0){                                      
                                      B=(B/T);
                                      
                                      tempB=tempB+1;
                                      ;                                      
                                      }
                          else T=T+1;                                                                                  
                          }             
transportador_mistico:
//obiamente es llamado asi por que este nos indica a  donde mandarlo , la primera opcion (a_u_e_p) es cuando al menos uno es primo
//la segunda (ninguno_es_p) es cuando ninguno es primo 
                      if(tempB==1) goto a_u_e_p;
                      if(tempC==1) goto a_u_e_p;
                      else goto ninguno_es_p;                      

a_u_e_p:
//entramos qui cuando B y/o A (que es llamado C) es primo lo que se hace aqui es ver si se puede dividir el numero primo entre el otro numero , esta fuera del alcanse de esta tarea 
//el explicar el por que el MCD=MCM cuando son iguales ,cuando son primos distintos o cuando uno es multiplo del otro

        if(tempB=1){
                    if (L%R==0){
                                printf("MCD=%d\nMCM=%d\nDe los numeros %d , %d\n", R, L, L, R);
                                goto fin_MCD;
                                }
                    if (R%L==0){
                                printf("MCD=%d\nMCM=%d\nDe los numeros %d , %d\n", L, R, L, R);
                                goto fin_MCD;
                                }
                    else{
                         C=L*R;
                         printf("MCD=1\nMCM=%d\nPara los numeros: %d , %d\n", C, R, L);
                         goto fin_MCD;
                         }     
                    }
        if(tempC=1){
                    if (L%R==0){
                                printf("MCD=%d\nMCM=%d\nDe los numeros %d , %d\n", R, L, L, R);
                                goto fin_MCD;
                                }
                    if (R%L==0){
                                printf("MCD=%d\nMCM=%d\nDe los numeros %d , %d\n", L, R, L, R);
                                goto fin_MCD;
                                }
                    else{
                         C=L*R;
                         printf("MCD=1\nMCM=%d\nPara los numeros: %d , %d\n", C, R, L);
                         goto fin_MCD;
                         }
                    }
                                                    
ninguno_es_p:
//aqui entramos cuando ninguno es primo
//los primeros 2 if son para verificar si uno es multiplo del otro                                                                 
             if (L%R==0){
                         printf("MCD=%d\nMCM=%d\nDe los numeros %d , %d\n", R, L, L, R);
                         goto fin_MCD;
                         }
             if (R%L==0){
                         printf("MCD=%d\nMCM=%d\nDe los numeros %d , %d\n", L, R, L, R);
                         goto fin_MCD;
                         }             
//esta es la parte que mas problemas me dio
//creo que no seria necesario toooooooooooooooooodooooooooo el codigo anterior
//segun yo con este else supongo que jalaria
//pero devido a que ya me habia cansado de que no jalara esta parte 
//en el momento que me di cuanta que jalo ya no quise mover nada
//se ha de mencionar que este fue el ultimo ejercicio que hice
             else{
                  Y=L+R;
                  O=L;
                  X=R;
                  P=2;
                  tempMCD=0;
                  MCD=1;
                  tempB=0;
                  while (P<Y){
                              if ((O%P==0)&&(X%P==0)){//entra aqui cada que ambos sean divisibles entre I
                                                     O=(O/P);
                                                     X=(X/P);
                                                     MCD=MCD*P;//el MCD
                                                     }
                              else P=P+1;
                              }
                  V=L*R;
                  MCM=(V/MCD);
                  printf("\nMCD=%d\nMCM=%d\nPara los numeros %d , %d\n", MCD, MCM, L, R);                                                                                         
                  }
                                                                          
fin_MCD:
        puts("Si desea continuar calculando MCM y MCD precione 0\nSi deseair al menu principal precione 1\nSi desea salir precione cualquier otro numero");
        scanf("%i", &seleccion_3);
        if (seleccion_3==0){
                            system("cls");
                            goto ejercicio_3;                            
                            } 
        if (seleccion_3==1){                            
                            system("cls"); 
                            goto precentacion;                            
                             }                                       
        else {
              system("cls");
              goto fin_general;              
              }                            

ejercicio_2:            
            int An, Sg, W, a, seleccion_2;

inicio_fivonnaci:
            puts("Cuantos terminos de la serie de Fivonnaci quiere ver?");
            scanf("%d", &a);
            getchar();
            goto verificar_a;

verificar_a:
            if (a<1){
                     puts("Porfavor elija un valor positivo"); 
                     goto ejercicio_4;
                     }
            else if (0<a){
                          if (a>46){//analogamente limitaciones 
                                    puts("Devido a limitaciones tendra que escojer a lo mas 46 como valor");
                                    goto ejercicio_2;
                                    }
                          goto calcular_fivonnaci;
                          }

calcular_fivonnaci:
                   An=1;//este es el termino anterior
                   Sg=1;//el siguinte
                   puts("Termino     Valor");//esto es para que se vea mas bonito 
                   for(W=0;W<a;W++){
                                    printf("%d %12d\n", W+1, An);
                                    Sg+=An;
                                    An=Sg-An; //esto es para que al volver a iniciar otra vez el ciclo inicie sumando el termino n-1 y no el n para el termino n+1
                                    }
                   puts("Si desea ver mas terminos de la serie de Fivonnaci precione 0\nSi deseair al menu principal precione 1\nSi desea salir precione cualquier otro numero");
                   scanf("%d", &seleccion_2);
                   if (seleccion_2==0){
                                       system("cls");
                                       goto ejercicio_2;                            
                                       } 
                   if (seleccion_2==1){
                                       system("cls");
                                       goto precentacion;
                                       }                                       
                   else {
                         system("cls");
                         goto fin_general;
                         }                                             
                               
ejercicio_5:
            int I, J, A[5][5], trA, cA[5][5], tr1, tr2, tr3, D, M[5][5], sump, prdp, K, seleccion_5;
            
inicio_matriz:
              puts("Calcularemos la\nTraza (tr(A))\nTranspuesta(A^t)\nCuadrado(A^2)\nDe una matriz A de 5x5\n");
              puts("a[1][1]  a[1][2]  a[1][3]  a[1][4]  a[1][5]\na[2][1]  a[2][2]  a[1][3]  a[2][4]  a[2][5]\na[3][1]  a[3][2]  a[3][3]  a[3][4]  a[3][5]\na[4][1]  a[4][2]  a[4][3]  a[4][4]  a[4][5]\na[5][1]  a[5][2]  a[5][3]  a[5][4]  a[5][5]");
              puts("Introdusca los elementos de la matriz\n");
              for(I=0;I<5;I++){
                               for(J=0;J<5;J++){
                                                printf("a[%d,%d]: ",I+1,J+1);
                                                scanf("%d",&A[I][J]);
                                                getchar();
                                                system("cls");
                                                }
                               }
              goto calcular_matriz;
            
calcular_matriz:
                trA=0;
                tr1=0;                
                for(I=0;I<5;I++){//esto calcula la traza que es sumar los terminos de la diagonal
                                 tr1=A[I][I];
                                 trA=trA+tr1;
                                 }
                for(I=0;I<5;I++){//esto es para calcular la multiplicacion de A*A
                                 for(J=0;J<5;J++){
                                                  sump=0;//aqui al iniciar el ciclo borra el resultdo del producto puto
                                                  prdp=0;//aqui tambien solo que aqui borra el resultado de la multiplicacion entrada por entrada
                                                  for(K=0;K<5;K++){//aqui hace el producto punto del renglon y la columna
                                                                   prdp=A[I][K]*A[K][J];
                                                                   sump=sump+prdp;
                                                                   }
                                                  M[I][J]=sump;//aqui guardamos el resultado del producto puntodel ciclo anterior en el elemento aij corespondiente
                                                  }
                                 }                            
                goto imprimir_matriz;
            
imprimir_matriz:
                printf("\n la matriz A es:\n");
                for(I=0;I<5;I++){
                                 for(J=0;J<5;J++){//esto es para imprimir los renglones
                                                  printf("%9d",A[I][J]);//el 9antes de la "d" me indica los espacios 
                                                  }
                                 printf("\n");//esto da un salto de linea al terminar el renglon
                                 }                            
                printf("\ntr(A)=%d\n", trA);
                printf("\n A^2 es:\n\n");
                for(I=0;I<5;I++){
                                 for(J=0;J<5;J++){
                                                  printf("%9d",M[I][J]);
                                                  }
                                 printf("\n");
                                 }
                puts("\nA^t es:\n");
                for(J=0;J<5;J++){//y como la transpuesta es simplemente cabiar renglones por columnas solo vasta con comenzar el ciclo con j y no con i
                                 for(I=0;I<5;I++){
                                                  printf("%9d",A[I][J]);
                                                  }
                                 printf("\n");
                                 }                                               
                puts("Si desea reasignale valores a la matriz precione 0\nSi deseair al menu principal precione 1\nSi desea salir precione cualquier otro numero");
                scanf("%d", &seleccion_5);
                  if (seleccion_5==0){
                                      system("cls");
                                      goto ejercicio_5;                            
                                      } 
                  if (seleccion_5==1){
                                      system("cls");
                                      goto precentacion;
                                      }                                       
                  else {
                        system("cls");
                        goto fin_general;
                        }               


fin_general:
            puts("Integrantes del equipo:\n1.-Alonso Villegas Humberto\n2.-Villa Armendariz Leslie");
            system("PAUSE");
            return 0;
}
